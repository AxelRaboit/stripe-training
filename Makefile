create-database: ## Create database
	symfony console doctrine:database:create
	symfony console d:m:m
	symfony console d:f:l

recreate-database: ## Reacrate database
	symfony console doctrine:database:drop --force
	symfony console doctrine:database:create
	symfony console d:m:m
	symfony console d:f:l