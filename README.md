# Installation
- composer install
- symfony console d:d:c
- symfony console d:m:m

# Create database
- make create-database

# Recreate database
- make recreate-database

## Register
- https://localhost:8000/register

## Login
- https://localhost:8000/login

## Access to database
- https://localhost:8000/adminer.php
[
    127.0.0.1 / localhost
    MySQL User
    MySQL Password
    DB: stripe
]

## Stripe Keys
- Ask to get them and put them into the env.local
- STRIPE_PUBLIC_KEY_TEST=xxx
- STRIPE_SECRET_KEY_TEST=xxx

## Bundles
- composer require --dev orm-fixtures (no more used)
- composer require stripe/stripe-php
- composer require symfony/webpack-encore-bundle

## Configuration
- Composer version 2.2.7
- PHP 7.4.28 (cli)
- Symfony 5

## After the installation
- Load stripe data to retrieve the stripe product