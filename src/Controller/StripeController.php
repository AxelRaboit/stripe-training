<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Order;
use App\Entity\Product;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StripeController extends AbstractController
{
    private $privateKey;
    private $em;
    private $productRepository;
    private $orderRepository;

    public function __construct(
        EntityManagerInterface $em,
        ProductRepository $productRepository,
        OrderRepository $orderRepository
    )
    {
        if($_ENV['APP_ENV']  === 'dev') {
            $this->privateKey = $_ENV['STRIPE_SECRET_KEY_TEST'];
        } else {
            $this->privateKey = $_ENV['STRIPE_SECRET_KEY_LIVE'];
        }

        $this->em = $em;
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @Route("/", name="products")
     */
    public function index(): Response
    {
        // Stripe Api Key retrieve
        \Stripe\Stripe::setApiKey($this->privateKey);

        // Getting the user
        $user = $this->getUser();
        
        // Retrieve all subscriptions related to the customer
        $userAllowedToSeeInvoicesButton = false;
        $userAllowedToSeeStripePortal = false;
        
        if($user->getCustomerStripeId() !== '' && $user->getCustomerStripeId() !== null){

            // Retrieve all invoices related to the customer
            $invoices = \Stripe\Invoice::all(['customer' => $user->getCustomerStripeId()]);
            // If the customer has invoices set variable to true
            if($invoices['data'] !== null || $invoices['data'] !== []) {
                $userAllowedToSeeInvoicesButton = true;
            }

            $subscriptions = \Stripe\Subscription::all(['customer' => $user->getCustomerStripeId()]);
            
            // If the customer has subscriptions set variable to true
            if($subscriptions['data'] !== []) {
                $userAllowedToSeeStripePortal = true;
            }
    
        }

        return $this->render('stripe/product/index.html.twig', [
            'products' => $this->productRepository->findAll(),
            'devise' => Order::DEVISE,
            'userAllowedToSeeStripePortal' => $userAllowedToSeeStripePortal,
            'userAllowedToSeeInvoicesButton' => $userAllowedToSeeInvoicesButton,
        ]);
    }

    /**
     * @Route("/product/{id}", name="product")
     */
    public function product(Product $product): Response
    {
        return $this->render('stripe/product/show.html.twig', [
            'product' => $product,
            'devise' => Order::DEVISE
        ]);
    }

    /**
     * @Route("/stripe/get-data/load", name="stripe_get_data")
     */
    public function uploadDataFromStripe(): Response
    {
        // Stripe Api Key retrieve
        \Stripe\Stripe::setApiKey($this->privateKey);

        // Get stripe products
        $stripeProducts = \Stripe\Product::all(['active' => true]);

        // Get stripe prices
        $stripePrices = \Stripe\Price::all(['active' => true]);

        // On parcours les stripe prices
        foreach ($stripePrices as $stripePrice) {
            // A chaque boucle des prices on récuprere sont produit associé
            $stripeProduct = \Stripe\Product::retrieve($stripePrice->product);

            // On cherche dans productRepository si un produit à le meme stripe id
            $dbProduct = $this->productRepository->findProductByProductStripeId($stripeProduct->id);

            if(!$dbProduct) {
                // Add stripe product to database
                $product = new Product;
                $product->setActive($stripeProduct->active === true ? true : false);
                $product->setName($stripeProduct->name);
                $product->setPriceIdStripe($stripePrice->id);
                $product->setDescription($stripeProduct->description);
                $product->setImage($stripeProduct['images'][0]);
                $product->setPrice($stripePrice->unit_amount / 100);
                if($stripePrice['type'] === 'recurring') {
                    $product->setPlan($stripePrice['recurring']['interval']);
                } else {
                    $product->setPlan($stripePrice['type']);
                }

                $product->setProductIdStripe($stripeProduct->id);

                $this->em->persist($product);
                $this->em->flush();

                // TODO Faire en sorte de mettre à jour si l'image change le prix change etc... pour mettre à jour en base de donnée
            } else {
                if($stripeProduct->active === false) {
                    $dbProduct[0]->setActive(false);
                } elseif($stripeProduct->active === true) {
                    $dbProduct[0]->setActive(true);
                }
                if($stripeProduct->name !== $dbProduct[0]->getName()) {
                    $dbProduct[0]->setName($stripeProduct->name);
                }
                if($stripeProduct->description !== $dbProduct[0]->getDescription()) {
                    $dbProduct[0]->setDescription($stripeProduct->description);
                }
                if(($stripePrice->unit_amount / 100) !== ($dbProduct[0]->getPrice())) {
                    $dbProduct[0]->setPrice($stripePrice->unit_amount / 100);
                    $dbProduct[0]->setPriceIdStripe($stripePrice->id);
                }
                if($stripeProduct['images'][0] !== $dbProduct[0]->getImage()) {
                    $dbProduct[0]->setImage($stripeProduct['images'][0]);
                }

                $this->em->persist($dbProduct[0]);
                $this->em->flush();


            }
        }

        return $this->redirectToRoute('products', [], 301);
    }

    /**
     * @Route("/stripe/create-checkout-session/product/{id}", name="checkout")
     */
    public function checkout(Product $product)
    {
        // Getting the user
        $user = $this->getUser();

        // Stripe Api Key retrieve
        \Stripe\Stripe::setApiKey($this->privateKey);


        // It is called 'one_time' or 'month' or 'year' because of the stripe data
        if ($product->getPlan() === 'one_time') {

            // RETRIEVE THE PRICE AND PRODUCT FROM STRIPE
            $stripePrice = \Stripe\Price::retrieve(
                $product->getPriceIdStripe(),
                []
            );

            // TODO enregistrer la CB du customer dans stripe
            
            // If the user has a customer stripe id into the database related to the customer stripe id
            if($user->getCustomerStripeId()){
                $customer = \Stripe\Customer::retrieve($user->getCustomerStripeId());

                /* If we want a condition in case that the customer has a customer id in the database but no more existing into stripe
                we need to use the search method to find the customer by his email
                Search is not supported on api version 2020-03-02. Update your API version, or set the API Version of this request to 2020-08-27 or greater. */

            }

            // If the user doesn't have a customer stripe id into the database and stripe (new user / customer)
            if (!$user->getCustomerStripeId()) {
                
                /* If we want a condition in case that the customer doesn't not have a customer id into the database but may be existing into stripe
                we need to use the search method to find the customer by his email
                Search is not supported on api version 2020-03-02. Update your API version, or set the API Version of this request to 2020-08-27 or greater. */

                // If the customer doesn't not have a customer id in the database and stripe
                $customer = \Stripe\Customer::create([ 'email' => $user->getEmail() ]);
                $user->setCustomerStripeId($customer->id);
                $this->em->persist($user);
                $this->em->flush();
            }
            
            // Session creation
            $session = \Stripe\Checkout\Session::create([
                'payment_method_types' => ['card'],
                'customer' => $customer->id,
                'mode' => 'payment',
                'line_items' => [[
                    'price' => $stripePrice->id,
                    'adjustable_quantity' => [
                        'enabled' => true,
                        'minimum' => 1,
                        'maximum' => 100,
                    ],
                    'quantity' => 1,
                ]],
                'success_url' => $this->generateUrl('stripe_validation', ['id' => $product->getId()], UrlGeneratorInterface::ABSOLUTE_URL)."?session_id={CHECKOUT_SESSION_ID}",
                'cancel_url' => $this->generateUrl('cancel', [], UrlGeneratorInterface::ABSOLUTE_URL)
            ]);
        } else if ($product->getPlan() === 'month') {

            // RETRIEVE THE PRICE AND PRODUCT FROM STRIPE
            $stripePrice = \Stripe\Price::retrieve(
                $product->getPriceIdStripe(),
                []
            );

            // RETRIEVE CUSTOMER IF EXIST
            if($user->getCustomerStripeId()){
                $customer= \Stripe\Customer::retrieve(
                    $user->getCustomerStripeId(),
                    []
                );
            } else {
                $customer = \Stripe\Customer::create([ 'email' => $user->getEmail() ]);
                $user->setCustomerStripeId($customer->id);
                $this->em->persist($user);
                $this->em->flush();
            }

            // Get the UNIX TimeStamp + 1 month
            $currentDateTime = new \DateTime('now');
            $timeStamp = date_timestamp_get($currentDateTime);
            $trialDuration = '+ 30 days';
            $trialEnd = strtotime($trialDuration, $timeStamp); 

            // Session creation
            $session = \Stripe\Checkout\Session::create([
                'payment_method_types' => ['card'],
                'customer' => $customer->id,
                'subscription_data' => ['trial_end' => $trialEnd],
                'mode' => 'subscription',
                'line_items' => [[
                    'price' => $stripePrice->id,
                    'adjustable_quantity' => [
                        'enabled' => true,
                        'minimum' => 1,
                        'maximum' => 100,
                    ],
                    // For metered billing, do not pass quantity
                    'quantity' => 1,
                ]],
                'success_url' => $this->generateUrl('stripe_validation', ['id' => $product->getId()], UrlGeneratorInterface::ABSOLUTE_URL)."?session_id={CHECKOUT_SESSION_ID}",
                'cancel_url' => $this->generateUrl('cancel', [], UrlGeneratorInterface::ABSOLUTE_URL),
            ]);
        } else if ($product->getPlan() === 'year') {

            // RETRIEVE THE PRICE AND PRODUCT FROM STRIPE
            $stripePrice = \Stripe\Price::retrieve(
                $product->getPriceIdStripe(),
                []
            );

            // RETRIEVE CUSTOMER IF EXIST
            if($user->getCustomerStripeId()){
                $customer= \Stripe\Customer::retrieve(
                    $user->getCustomerStripeId(),
                    []
                );
            } else {
                $customer = \Stripe\Customer::create([ 'email' => $user->getEmail() ]);
                $user->setCustomerStripeId($customer->id);
                $this->em->persist($user);
                $this->em->flush();
            }

            // Get the UNIX TimeStamp + 1 month
            $currentDateTime = new \DateTime('now');
            $timeStamp = date_timestamp_get($currentDateTime);
            $trialDuration = '+ 30 days';
            $trialEnd = strtotime($trialDuration, $timeStamp); 

            // Session creation
            $session = \Stripe\Checkout\Session::create([
                'payment_method_types' => ['card'],
                'customer' => $customer->id,
                'subscription_data' => ['trial_end' => $trialEnd],
                'mode' => 'subscription',
                'line_items' => [[
                'price' => $stripePrice->id,
                'adjustable_quantity' => [
                    'enabled' => true,
                    'minimum' => 1,
                    'maximum' => 100,
                  ],
                // For metered billing, do not pass quantity
                'quantity' => 1,
                ]],
                /* 'success_url' => "http://127.0.0.1:8000/stripe/validation/session?session_id={CHECKOUT_SESSION_ID}", */
                'success_url' => $this->generateUrl('stripe_validation', ['id' => $product->getId()], UrlGeneratorInterface::ABSOLUTE_URL)."?session_id={CHECKOUT_SESSION_ID}",
                'cancel_url' => $this->generateUrl('cancel', [], UrlGeneratorInterface::ABSOLUTE_URL),
            ]);
            
        }

        return $this->redirect($session->url, 303);
    }

    /**
     * @Route("/stripe/validation/product/{id}", name="stripe_validation")
     */
    public function stripeValidation(Request $request, $id)
    {   
        // Getting the user
        $user = $this->getUser();

        // Find the product
        $product = $this->productRepository->find($id);

        // Stripe Api Key retrieve
        \Stripe\Stripe::setApiKey($this->privateKey);

        // Get the created session
        $retrieveSession = \Stripe\Checkout\Session::retrieve($request->get('session_id'));

        // Get the customer related to the session
        /* $customer = \Stripe\Customer::retrieve($retrieveSession->customer); */

        // Order creation
        $order = new Order;
        $order->setUser($user);
        $order->setCreatedAt(new \DateTime('now'));
        $order->setUpdateAt(new \DateTime('now'));
        $order->setSessionStripe($retrieveSession->id);
        $order->setReference(uniqid());
        $order->setUnitPrice($product->getPrice());
        $order->setProduct($product);
        $order->setPaid(true);

        //Usefull to debug
        /* dd($session); */

        if(null !== $retrieveSession->payment_intent) {
            // Case of PaymentIntent (One time payment)
            $paymentIntent = \Stripe\PaymentIntent::retrieve($retrieveSession->payment_intent);
            //Usefull to debug
            /* dd($paymentIntent); */
            $order->setStripeToken($paymentIntent->client_secret);
            $order->setCustomerStripe($paymentIntent->customer);
            $order->setBrandStripe($paymentIntent['charges']['data'][0]['payment_method_details']['card']['brand']);
            $order->setLast4Stripe($paymentIntent['charges']['data'][0]['payment_method_details']['card']['last4']);
            $order->setIdChargeStripe($paymentIntent['charges']['data'][0]['id']);
            $order->setStatusStripe($paymentIntent['charges']['data'][0]['status']);
            $order->setPrice($paymentIntent->amount / 100);
            $order->setPaymentType('oneTimePayment');
            $order->setQuantity(($paymentIntent['charges']['data'][0]['amount'] / 100) / ($product->getPrice()));
            $order->setFinish(true);

        } elseif(null !== $retrieveSession->subscription) {
            // Case of Subscription (If this a subscription)

            $subscription = \Stripe\Subscription::retrieve($retrieveSession->subscription,[]);
            //Usefull to debug
            /* dd($subscription); */
            $order->setPaymentType('subscription');
            $order->setCustomerStripe($subscription->customer);
            $order->setLatestInvoiceStripe($subscription->latest_invoice);
            $order->setStatusStripe($subscription['status']);
            $order->setPrice($subscription['items']['data'][0]['price']['unit_amount'] / 100 * ($subscription['items']['data'][0]['quantity']));
            $order->setQuantity($subscription['items']['data'][0]['quantity']);
        }

        $this->em->persist($order);
        $this->em->flush();

        return $this->redirectToRoute('success_landing', ['sessionId' => $retrieveSession->id], 301);
    }
    
    /**
     * @Route("/stripe/success/landing/session/{sessionId}", name="success_landing")
     */
    public function successLanding($sessionId)
    {
        // Getting the user
        $user = $this->getUser();

        // Get the order to display the button to manager the subscription into the success page
        $order = $this->orderRepository->findOneBy([ 'sessionStripe' => $sessionId]);

        if (!$order) {
            return $this->redirectToRoute('products');
        }

        return $this->render('stripe/payment/success.html.twig', [
            'user' => $user,
            'order' => $order
        ]);
    }

    /**
     * @Route("/cancel", name="cancel")
     */
    public function cancel()
    {
        /* TODO Si la commande n'a pas été effectué completement alors 
        on reprend le order et on supprime pour dans la base de donnée et dans stripe. ???? */

        return $this->render('stripe/payment/cancel.html.twig', [
            
        ]);
    }

    /**
     * @Route("/stripe/customer/portal", name="customer_portal")
     */
    public function customer_portal()
    {
        $user = $this->getUser();

        // Stripe Api Key retrieve
        \Stripe\Stripe::setApiKey($this->privateKey);
        
        // Get the order
        $order = $this->orderRepository->findOneBy([ 'user' => $user->getId() ], ['id' => 'DESC']);
        
        // Retrieve the Customer and save to acces to the portal
        $customer = \Stripe\Customer::retrieve($order->getCustomerStripe());
        $customer->save();

        // Retrieve the stripe session
        $retrievedSession = \Stripe\Checkout\Session::retrieve($order->getSessionStripe());

        // Get the customer frop the stripe session
        $customer = $retrievedSession->customer;

        // Portal creation
        $session = \Stripe\BillingPortal\Session::create([
            'customer' => $customer,
            'return_url' => $this->generateUrl('payment_return', ['subscriptionId' => $retrievedSession->subscription, 'orderId' => $order->getId()], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        return $this->redirect($session->url, 303);

    }

    /**
     * @Route("/stripe/order/return/{subscriptionId}/{orderId}", name="payment_return")
     */
    public function returnSubscription($subscriptionId, $orderId): Response
    {
        // Stripe Api Key retrieve
        \Stripe\Stripe::setApiKey($this->privateKey);
        
        $plan = \Stripe\Subscription::retrieve($subscriptionId);

        if($plan->cancel_at_period_end === true){
            $order = $this->orderRepository->findOneBy([ 'id' => $orderId ]);
            
            $date = new \DateTime();
            $date->setTimestamp($plan->cancel_at);

            $order->setDateEndAt($date);
            $order->setFinish(true);
            $this->em->persist($order);

            $this->em->flush();
        }
        else{
            return $this->redirectToRoute('products');
        }

        return $this->render('stripe/payment/return.html.twig'); 
    }

    /**
     * @Route("/stripe/invoices", name="invoices")
     */
    public function invoices(): Response
    {
        // Getting the user
        $user = $this->getUser();

        // Stripe Api Key retrieve
        \Stripe\Stripe::setApiKey($this->privateKey);

        // Getting customer stripe id
        $customerStripeId = $user->getCustomerStripeId();

        // Retrieve all invoices related to the customer and a subscription
        $invoices = \Stripe\Invoice::all(['customer' => $customerStripeId]);

        // Retrieve all PaymentIntents which is finished payment (subscription and one time payment)
        $paymentIntents = \Stripe\PaymentIntent::all(['customer' => $customerStripeId]);

        return $this->render('stripe/invoice/index.html.twig', [
            'invoices' => $invoices['data'],
            'paymentIntents' => $paymentIntents['data']
        ]);
    }

}
