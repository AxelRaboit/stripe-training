<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait OrderStripeTrait
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stripeToken;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $brandStripe;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $last4Stripe;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $idChargeStripe;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $statusStripe;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sessionStripe;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $customerStripe;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $latestInvoiceStripe;

    /**
     * @return null|string
     */
    public function getStripeToken(): ?string
    {
        return $this->stripeToken;
    }

    /**
     * @param null|string $stripeToken
     */
    public function setStripeToken(?string $stripeToken): void
    {
        $this->stripeToken = $stripeToken;
    }

    /**
     * @return null|string
     */
    public function getBrandStripe(): ?string
    {
        return $this->brandStripe;
    }

    /**
     * @param null|string $brandStripe
     */
    public function setBrandStripe(?string $brandStripe): void
    {
        $this->brandStripe = $brandStripe;
    }

    /**
     * @return null|string
     */
    public function getLast4Stripe(): ?string
    {
        return $this->last4Stripe;
    }

    /**
     * @param null|string $last4Stripe
     */
    public function setLast4Stripe(?string $last4Stripe): void
    {
        $this->last4Stripe = $last4Stripe;
    }

    /**
     * @return null|string
     */
    public function getIdChargeStripe(): ?string
    {
        return $this->idChargeStripe;
    }

    /**
     * @param null|string $idChargeStripe
     */
    public function setIdChargeStripe(?string $idChargeStripe): void
    {
        $this->idChargeStripe = $idChargeStripe;
    }

    /**
     * @return null|string
     */
    public function getStatusStripe(): ?string
    {
        return $this->statusStripe;
    }

    /**
     * @param null|string $statusStripe
     */
    public function setStatusStripe(?string $statusStripe): void
    {
        $this->statusStripe = $statusStripe;
    }

    /**
     * @return null|string
     */
    public function getSessionStripe(): ?string
    {
        return $this->sessionStripe;
    }

    /**
     * @param null|string $sessionStripe
     */
    public function setSessionStripe(?string $sessionStripe): void
    {
        $this->sessionStripe = $sessionStripe;
    }

    /**
     * @return null|string
     */
    public function getCustomerStripe(): ?string
    {
        return $this->customerStripe;
    }

    /**
     * @param null|string $customerStripe
     */
    public function setCustomerStripe(?string $customerStripe): void
    {
        $this->customerStripe = $customerStripe;
    }

    /**
     * @return null|string
     */
    public function getLatestInvoiceStripe(): ?string
    {
        return $this->latestInvoiceStripe;
    }

    /**
     * @param null|string $latestInvoiceStripe
     */
    public function setLatestInvoiceStripe(?string $latestInvoiceStripe): void
    {
        $this->latestInvoiceStripe = $latestInvoiceStripe;
    }
}