<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait ProductStripeTrait
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $productIdStripe;

    /**
     * @return null|string
     */
    public function getProductIdStripe(): ?string
    {
        return $this->productIdStripe;
    }

    /**
     * @param null|string $productIdStripe
     */
    public function setProductIdStripe(?string $productIdStripe): void
    {
        $this->productIdStripe = $productIdStripe;
    }
}